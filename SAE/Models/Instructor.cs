﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAE.Models
{
    public class Instructor
    {
        public int ID { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Materia { get; set; }
    }
}